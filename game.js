const tile_size = 96;
const teleport_distance = 11*(tile_size**2);
const jump_moment = tile_size*-0.10;
const gravity = tile_size*0.002;
const horizontal_moment = tile_size*0.03;
const max_moment = tile_size*0.10;
const climb_speed = tile_size*-0.02;

let map;

const wall_texture = PIXI.Texture.from('assets/graphics/platformIndustrial_060.png');
const bg_texture = PIXI.Texture.from('assets/graphics/platformIndustrial_094.png');
const block_texture = PIXI.Texture.from('assets/graphics/platformIndustrial_058.png');
const player_texture = PIXI.Texture.from('assets/graphics/idle/frame_00.png');
const fail_texture = PIXI.Texture.from('assets/graphics/fail.png');
const success_texture = PIXI.Texture.from('assets/graphics/success.png');

PIXI.sound.Sound.from({
	url: 'assets/sounds/bg_sound.wav',
	autoPlay: true,
	loop: true,
	volume: 0.5
});
PIXI.sound.add('jump', {url :'assets/sounds/jump_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('dig', {url :'assets/sounds/dig_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('powerup', {url :'assets/sounds/powerup_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('teleport', {url :'assets/sounds/teleport_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('fail', {url :'assets/sounds/fail_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('success', {url :'assets/sounds/success_sound.wav', autoPlay: false, loop: false});
PIXI.sound.add('climb', {url :'assets/sounds/climb_sound.wav', autoPlay: true, loop: true, complete: () => {
	PIXI.sound.pause('climb');
}});

/*const bg_sound = new Audio('assets/sounds/bg_sound.wav');
const jump_sound = new Audio('assets/sounds/jump_sound.wav');
const dig_sound = new Audio('assets/sounds/dig_sound.wav');
const powerup_sound = new Audio('assets/sounds/powerup_sound.wav');
const teleport_sound = new Audio('assets/sounds/teleport_sound.wav');
const fail_sound = new Audio('assets/sounds/fail_sound.wav');
const success_sound = new Audio('assets/sounds/success_sound.wav');
const climb_sound = new Audio('assets/sounds/climb_sound.wav');*/

const player_idle_textures = [
	PIXI.Texture.from('assets/graphics/idle/frame_00.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_01.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_02.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_03.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_04.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_05.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_06.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_07.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_08.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_09.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_10.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_11.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_12.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_13.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_14.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_15.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_16.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_17.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_18.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_19.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_20.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_21.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_22.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_23.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_24.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_25.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_26.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_27.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_28.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_29.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_30.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_31.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_32.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_33.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_34.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_35.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_36.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_37.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_38.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_39.png'),
	PIXI.Texture.from('assets/graphics/idle/frame_40.png'),
];

const player_lidle_textures = [
	PIXI.Texture.from('assets/graphics/left_idle/frame_00.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_01.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_02.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_03.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_04.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_05.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_06.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_07.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_08.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_09.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_10.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_11.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_12.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_13.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_14.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_15.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_16.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_17.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_18.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_19.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_20.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_21.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_22.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_23.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_24.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_25.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_26.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_27.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_28.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_29.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_30.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_31.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_32.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_33.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_34.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_35.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_36.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_37.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_38.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_39.png'),
	PIXI.Texture.from('assets/graphics/left_idle/frame_40.png'),
];

const player_run_textures = [
	PIXI.Texture.from('assets/graphics/run/frame_00.png'),
	PIXI.Texture.from('assets/graphics/run/frame_01.png'),
	PIXI.Texture.from('assets/graphics/run/frame_02.png'),
	PIXI.Texture.from('assets/graphics/run/frame_03.png'),
	PIXI.Texture.from('assets/graphics/run/frame_04.png'),
	PIXI.Texture.from('assets/graphics/run/frame_05.png'),
	PIXI.Texture.from('assets/graphics/run/frame_06.png'),
	PIXI.Texture.from('assets/graphics/run/frame_07.png'),
	PIXI.Texture.from('assets/graphics/run/frame_08.png'),
	PIXI.Texture.from('assets/graphics/run/frame_09.png'),
	PIXI.Texture.from('assets/graphics/run/frame_10.png'),
	PIXI.Texture.from('assets/graphics/run/frame_11.png'),
	PIXI.Texture.from('assets/graphics/run/frame_12.png'),
	PIXI.Texture.from('assets/graphics/run/frame_13.png'),
	PIXI.Texture.from('assets/graphics/run/frame_14.png'),
	PIXI.Texture.from('assets/graphics/run/frame_15.png'),
	PIXI.Texture.from('assets/graphics/run/frame_16.png'),
	PIXI.Texture.from('assets/graphics/run/frame_17.png'),
	PIXI.Texture.from('assets/graphics/run/frame_18.png'),
	PIXI.Texture.from('assets/graphics/run/frame_19.png'),
	PIXI.Texture.from('assets/graphics/run/frame_20.png'),
	PIXI.Texture.from('assets/graphics/run/frame_21.png'),
	PIXI.Texture.from('assets/graphics/run/frame_22.png')
];

const player_left_textures = [
	PIXI.Texture.from('assets/graphics/left/frame_00.png'),
	PIXI.Texture.from('assets/graphics/left/frame_01.png'),
	PIXI.Texture.from('assets/graphics/left/frame_02.png'),
	PIXI.Texture.from('assets/graphics/left/frame_03.png'),
	PIXI.Texture.from('assets/graphics/left/frame_04.png'),
	PIXI.Texture.from('assets/graphics/left/frame_05.png'),
	PIXI.Texture.from('assets/graphics/left/frame_06.png'),
	PIXI.Texture.from('assets/graphics/left/frame_07.png'),
	PIXI.Texture.from('assets/graphics/left/frame_08.png'),
	PIXI.Texture.from('assets/graphics/left/frame_09.png'),
	PIXI.Texture.from('assets/graphics/left/frame_10.png'),
	PIXI.Texture.from('assets/graphics/left/frame_11.png'),
	PIXI.Texture.from('assets/graphics/left/frame_12.png'),
	PIXI.Texture.from('assets/graphics/left/frame_13.png'),
	PIXI.Texture.from('assets/graphics/left/frame_14.png'),
	PIXI.Texture.from('assets/graphics/left/frame_15.png'),
	PIXI.Texture.from('assets/graphics/left/frame_16.png'),
	PIXI.Texture.from('assets/graphics/left/frame_17.png'),
	PIXI.Texture.from('assets/graphics/left/frame_18.png'),
	PIXI.Texture.from('assets/graphics/left/frame_19.png'),
	PIXI.Texture.from('assets/graphics/left/frame_20.png'),
	PIXI.Texture.from('assets/graphics/left/frame_21.png'),
	PIXI.Texture.from('assets/graphics/left/frame_22.png')
];

const player_attack_textures = [
	PIXI.Texture.from('assets/graphics/attack/frame_00.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_01.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_02.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_03.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_04.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_05.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_06.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_07.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_08.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_09.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_10.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_11.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_12.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_13.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_14.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_15.png'),
	PIXI.Texture.from('assets/graphics/attack/frame_16.png')
];

const player_lattack_textures = [
	PIXI.Texture.from('assets/graphics/attack_left/frame_00.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_01.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_02.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_03.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_04.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_05.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_06.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_07.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_08.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_09.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_10.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_11.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_12.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_13.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_14.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_15.png'),
	PIXI.Texture.from('assets/graphics/attack_left/frame_16.png')
];

const player_jump_textures = [
	PIXI.Texture.from('assets/graphics/jump/frame_00.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_01.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_02.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_03.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_04.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_05.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_06.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_07.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_08.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_09.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_10.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_11.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_12.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_13.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_14.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_15.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_16.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_17.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_18.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_19.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_20.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_21.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_22.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_23.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_24.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_25.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_26.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_27.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_28.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_29.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_30.png'),
	PIXI.Texture.from('assets/graphics/jump/frame_31.png')
];

const powerup_textures = {
	left : PIXI.Texture.from('assets/graphics/left.png'),
	dig : PIXI.Texture.from('assets/graphics/dig.png'),
	jump : PIXI.Texture.from('assets/graphics/jump.png'),
	teleport : PIXI.Texture.from('assets/graphics/teleport.png'),
	climb : PIXI.Texture.from('assets/graphics/climb.png'),
	double : PIXI.Texture.from('assets/graphics/double.png'),
};

const popups = {
	left : PIXI.Texture.from('assets/graphics/left_pop_up.png'),
	dig : PIXI.Texture.from('assets/graphics/dig_pop_up.png'),
	jump : PIXI.Texture.from('assets/graphics/jump_pop_up.png'),
	teleport : PIXI.Texture.from('assets/graphics/teleport_pop_up.png'),
	climb : PIXI.Texture.from('assets/graphics/climb_pop_up.png'),
	double : PIXI.Texture.from('assets/graphics/double_pop_up.png'),
	success : PIXI.Texture.from('assets/graphics/success.png'),
	fail : PIXI.Texture.from('assets/graphics/fail.png')
};

const pointer_texture = PIXI.Texture.from('assets/graphics/pointer_catcher.png');
const pointer_catcher = new PIXI.Sprite(pointer_texture);
pointer_catcher.anchor.set(0.5);
pointer_catcher.x = 480;
pointer_catcher.y = 360;
pointer_catcher.width = 960;
pointer_catcher.height = 720;

let player;

const scroll_container = new PIXI.Container();

let powerups = [];
let walls = [];
let blocks = [];

class Player extends PIXI.Sprite{
	constructor(right_idle_textures, left_idle_textures ,run_textures, left_textures, jump_textures, attack_textures, lattack_textures){
		super(player_texture);
		this.right_idle_textures = right_idle_textures;
		this.left_idle_textures = left_idle_textures;
		this.idle_textures = this.right_idle_textures;
		this.run_textures = run_textures;
		this.left_textures = left_textures;
		this.jump_textures = jump_textures;
		this.right_attack_textures = attack_textures;
		this.left_attack_textures = lattack_textures;
		this.attack_textures = this.right_attack_textures;
		this.moveId = 0;
		this.step = 0;
		this.beforeMoveId = 0;

		this.x = 480;
		this.y = 360;
		this.width = tile_size/2;
		this.height = tile_size;
		this.anchor.set(0.5);

		this.moment = {
			x : 0,
			y : 0
		};
		this.pos = {
			x : this.x-scroll_container.x,
			y : this.y-scroll_container.y
		};

		this.climb = false;
		this.dig = false;
		this.left = false;
		this.max_jumps = 0;
		this.teleport = false;
		this.climb_flag = false;
		this.jumps = 0;
	}
	move() {
		if(this.beforeMoveId != this.moveId){
			this.beforeMoveId = this.moveId;
			this.step = 0;
		}
		if(this.moveId == 0){
			this.texture = this.idle_textures[this.step];
			this.step = (this.step + 1) % this.idle_textures.length;
		}else if(this.moveId == 1){
			this.texture = this.run_textures[this.step];
			this.step = (this.step + 1) % this.run_textures.length;
			this.idle_textures = this.right_idle_textures;
			this.attack_textures = this.right_attack_textures;
		}else if(this.moveId == 2){
			this.texture = this.left_textures[this.step];
			this.step = (this.step + 1) % this.left_textures.length;
			this.idle_textures = this.left_idle_textures;
			this.attack_textures = this.left_attack_textures;
		}else if(this.moveId == 3){
			this.texture = this.attack_textures[this.step];
			this.step = (this.step + 1) % this.attack_textures.length;
			this.moment.x = 0;
		}
	}
	update() {
		if (this.moment.y < max_moment) {
			this.moment.y += gravity;
		}
		this.pos.y += this.moment.y;

		this.pos.x += this.moment.x;

		for (let i = 0; i < walls.length; i++) {
			if (testCol(player,walls[i])) {
				player.wallContact(walls[i]);
			}
		}
		for (let i = 0; i < blocks.length; i++) {
			if (testCol(player,blocks[i])) {
				player.blockContact(blocks[i]);
			}
		}
		for (let i = 0; i < powerups.length; i++) {
			if (testCol(player,powerups[i])) {
				player.powerupContact(powerups[i]);
			}
		}
		if (!this.climb_flag) {
			PIXI.sound.pause('climb');
		}

		scroll_container.x = this.x-this.pos.x;
		scroll_container.y = this.y-this.pos.y;

		if (this.pos.y > 7250) {
			pop_up_inf("fail");
			PIXI.sound.play('fail');
			player.update = none;
			player.move = none;
			setTimeout(() => {window.location.reload();}, 2000);
		} else if (this.pos.x < 440 && this.pos.y < 865) {
			pop_up_inf("success");
			PIXI.sound.play('success');
			player.update = none;
			player.move = none;
			setTimeout(() => {window.location.reload();}, 2000);
		}
	}

	jump() {
		if (this.jumps < this.max_jumps) {
			PIXI.sound.play('jump');
			this.moment.y = jump_moment;
			this.jumps += 1;
		}
	}

	powerupContact(powerup) {
		pop_up_inf(powerup.type);
		PIXI.sound.play('powerup');
		if (powerup.type === "jump" || powerup.type === "double") {
			this.max_jumps += 1;
		} else {
			this[powerup.type] = true;
		}
		powerups.splice(powerups.indexOf(powerup),1);
		scroll_container.removeChild(powerup);
	}

	wallContact(wall) {
		if (this.pos.y > wall.y + this.height/2 + wall.height/2 - max_moment*2
		&& this.pos.y < wall.y + this.height/2 + wall.height/2
		&& Math.abs(this.pos.x-wall.x) < this.width/2+wall.width/2) {
			if (this.climb_flag) {
				this.climb_flag = false;
			} else {
				this.pos.y = wall.y + this.height/2 + wall.height/2;
				this.moment.y = 0;
			}
		} else if (this.pos.y < wall.y - this.height/2 - wall.height/2 + max_moment*2
		&& this.pos.y > wall.y - this.height/2 - wall.height/2
		&& Math.abs(this.pos.x-wall.x) < this.width/2+wall.width/2) {
			this.pos.y = wall.y - this.height/2 - wall.height/2;
			this.moment.y = 0;
			this.jumps = 0;
		} else {
			if (this.pos.x > wall.x) {
				this.pos.x = wall.x + this.width/2 + wall.width/2;;
			} else {
				this.pos.x = wall.x - this.width/2 - wall.width/2;;
			}
			if (this.climb) {
				this.climb_flag = true;
				PIXI.sound.resume('climb');
				this.pos.y += climb_speed;
				this.moment.y = 0;
				this.jumps = this.max_jumps + 1;
			}
		}
	}

	blockContact(block) {
		if (this.dig) {
			if (this.pos.y > block.y + this.height/2 + block.height/2 - max_moment*2
			&& this.pos.y < block.y + this.height/2 + block.height/2
			&& Math.abs(this.pos.x-block.x) < this.width/2+block.width/2) {
				if (this.climb_flag) {
					this.climb_flag = false;
				} else {
					this.pos.y = block.y + this.height/2 + block.height/2;
					this.moment.y = 0;
				}
			} else {
				PIXI.sound.play('dig');
				player.moveId = 3;
				blocks.splice(blocks.indexOf(block),1);
				setTimeout(function(){
					scroll_container.addChild(new Bg(block.x,block.y));
					scroll_container.removeChild(block);
					this.moveId = 0;
				}.bind(this), 650);
			}
		} else {
			this.wallContact(block);
		}
	}

	teleport_func(x,y) {
		//console.log(x,y);
		if (this.teleport && this.jumps === 0 && (this.pos.x-x)**2+(this.pos.y-y)**2 < teleport_distance) {
			let old_pos = {
				x: this.pos.x,
				y: this.pos.y
			}

			this.pos.x = x;
			this.pos.y = y;

			let flag = true;
			for (let i = 0; i < walls.length && flag; i++) {
				if (testCol(player,walls[i])) {
					flag = false;
				}
			}
			for (let i = 0; i < blocks.length && flag; i++) {
				if (testCol(player,blocks[i])) {
					flag = false;
				}
			}

			if (flag) {
				this.jumps = this.max_jumps+1;
				PIXI.sound.play('teleport');
			} else {
				this.pos.x = old_pos.x;
				this.pos.y = old_pos.y;
			}
		}
	}
}

class Wall extends PIXI.Sprite {
	constructor(x ,y) {
		super(wall_texture);
		this.x = x;
		this.y = y;
		this.width = tile_size;
		this.height = tile_size;
		this.anchor.set(0.5);
		scroll_container.addChild(this);
		walls.push(this);
	}
}

class Bg extends PIXI.Sprite {
	constructor(x ,y) {
		super(bg_texture);
		this.x = x;
		this.y = y;
		this.width = tile_size;
		this.height = tile_size;
		this.anchor.set(0.5);
		scroll_container.addChild(this);
	}
}

class Block extends PIXI.Sprite {
	constructor(x ,y) {
		super(block_texture);
		this.x = x;
		this.y = y;
		this.width = tile_size;
		this.height = tile_size;
		this.anchor.set(0.5);
		scroll_container.addChild(this);
		blocks.push(this);
	}
}

class Powerup extends PIXI.Sprite {
	constructor(x, y, type, bg) {
		super(powerup_textures[type]);
		this.x = x;
		this.y = y;
		this.width = tile_size/2;
		this.height = tile_size/2;
		this.anchor.set(0.5);
		this.type = type;
		if (bg) {
			new Bg(x,y);
		}
		scroll_container.addChild(this);
		powerups.push(this);
	}
}

function pop_up_inf(type){
	let popup = new PIXI.Sprite(popups[type]);
	popup.x = 480;
	popup.y = 120;
	popup.anchor.set(0.5);
	popup.width = 426;
	popup.height = 240;
	//console.log(popup.x, popup.y);
	app.stage.addChild(popup);
	setTimeout(function(){
		app.stage.removeChild(popup);
	}, 2000)
}

function none(x,y) {
	// Intentionally Empty
}

function wall(x,y) {
	return new Wall(x,y);
}

function bg(x,y) {
	return new Bg(x,y);
}

function block(x,y) {
	return new Block(x,y);
}

function powerup(x,y,type,bg) {
	return new Powerup(x,y,type,bg);
}

function testCol(player, object) {
	return player.pos.x - player.width/2 < object.x + object.width/2
		&& player.pos.x + player.width/2 > object.x - object.width/2
		&& player.pos.y - player.height/2 < object.y + object.height/2
		&& player.pos.y + player.height/2 > object.y - object.height/2;
}

function populate() {
	let xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) {
			map = JSON.parse(xhr.response);
			for (let i = 0; i < map.data.length; i++) {
				for (let j = 0; j < map.data[0].length; j++) {
					let temp = map.data[i][j];
					if (temp) {
						window[map.functions[temp][0]](j*tile_size, i*tile_size,...map.functions[temp].slice(1));
					}
				}
			}
		}
	};
	xhr.open("GET", "assets/map/map.json", true);
	xhr.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
	xhr.send();
}

window.onload = function () {
	app = new PIXI.Application({ width: 960, height: 720, backgroundColor: 0x448888, resolution: 1 });
	document.body.appendChild(app.view);
	app.stage.addChild(pointer_catcher);
	app.stage.addChild(scroll_container);

	populate();

	scroll_container.x = -39*tile_size;
	scroll_container.y = -50.25*tile_size;

	player = new Player(player_idle_textures, player_lidle_textures, player_run_textures, player_left_textures, player_jump_textures, player_attack_textures, player_lattack_textures);
	app.stage.addChild(player);

	window.addEventListener("keydown", (event) => {
		switch(event.key) {
			case 'w':
				player.jump();
				break;
			case 's':

				break;
			case 'a':
				if (player.left && player.moveId != 3) {
					player.moment.x = -horizontal_moment;
					player.moveId = 2;
				}
				break;
			case 'd':
				if(player.moveId != 3){
					player.moment.x = horizontal_moment;
					player.moveId = 1;
				}

		}
	});
	window.addEventListener("keyup", (event) => {
		switch(event.key) {
			case 'w':

				break;
			case 's':

				break;
			case 'a':
				if(player.moveId != 3){
					player.moment.x = 0;
					player.moveId = 0;
				}
				break;
			case 'd':
			if(player.moveId != 3){
				player.moment.x = 0;
				player.moveId = 0;
			}
		}
	});
	pointer_catcher.interactive = true;
	pointer_catcher.on('pointerdown', (event) => {
		player.teleport_func(event.data.global.x-scroll_container.x,event.data.global.y-scroll_container.y);
	});

	/*app.ticker.add( (delta) => {

	});*/
	player.interval = setInterval(function(){
		player.move();
		player.update();
	}, 2000 / 60);
}
